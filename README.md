# CSV --> DB using Python

The program accepts CSV file and inserts the data into sqlite database.

The name of database is `csv.db` and contents are inserted into
`csv_file_data` table.

The name of database and table can be changed in `source/common_constants.py`

#### Example CSV File:
    Country,Age,Salary,Purchased
    France ,44 ,72000 ,No
    Spain  ,27 ,48000 ,Yes
    Germany,45 ,54000 ,No
    
#### Steps to install, run, test

1. Clone the project using `git clone https://kishan03@bitbucket.org/kishan03/csv_to_db_python.git` 

2. `cd` to project directory

3. Install: `./setup.sh`

3. Run: `./source/main.py <fullpath_to_csv_file>`

4. Tests: `pytest` or `python -m unittest`