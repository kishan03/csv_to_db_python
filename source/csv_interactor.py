#!/usr/bin/python3
import csv


class CsvOperations:

    def __init__(self):
        pass

    def read_csv_data(self, path_to_file):
        try:
            with open(path_to_file, 'r') as file_to_read:
                csv_reader = csv.reader(file_to_read)
                file_data = file_to_read.read()
                if not file_data:
                    print("File is empty! Exiting program....")
                    return
                # if file has header ignore it
                file_to_read.seek(0)
                # Todo: check until we hit first line if file has blank lines in begining.
                has_header = csv.Sniffer().has_header(file_to_read.readline())
                file_to_read.seek(0)
                if has_header:
                    print("The CSV file has header so ignoring it!")
                    next(csv_reader)  # skip header

                # validate file rows
                validated_csv_data = self.validate_csv_content(csv_reader)
                return validated_csv_data
        except IOError as e:
            print(e)
            return

    def validate_csv_content(self, csv_reader):
        """
        Returns on valid csv data which fits the schema format.
            :param csv_reader:
            :returns list of validated data:
        """
        validated_csv_data = []
        for row in csv_reader:
            row = [item.strip() for item in row if item]
            if self.is_valid(row):
                # Todo: Improve validation for each column.
                validated_csv_data.append(row)
        return validated_csv_data

    @staticmethod
    def is_valid(row=None):
        """
        Check each row has enough number of values and each value is of expected data type.
        Expected datatype for ['country','age', 'salary', 'purchased'] is 'str'
        :param row:
        :return: bool
        """
        if not row or type(row) != list or len(row) != 4:
            return False
        if [type(item) for item in row] != [str] * 4:
            return False
        return True
