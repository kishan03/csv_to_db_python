import sqlite3

from common_constants import DATABASE_NAME
from sql_queries import SQLITE_CREATE_TABLE_SQL, INSERT_INTO_SQL


def get_db_connection(database_name):
    connection = sqlite3.connect(database_name)
    return connection


def create_tabel_if_not_exists(connection, create_table_sql):
    cursor = connection.cursor()
    cursor.execute(create_table_sql)


def store_csv_data(data):
    connection = get_db_connection(database_name=DATABASE_NAME)
    create_tabel_if_not_exists(connection, SQLITE_CREATE_TABLE_SQL)
    cursor = connection.cursor()
    cursor.executemany(INSERT_INTO_SQL, data)
    connection.commit()
    connection.close()
