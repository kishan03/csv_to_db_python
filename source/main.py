#!/usr/bin/python3
import sys

from csv_interactor import CsvOperations
from db_interactor import store_csv_data


def main():
    if len(sys.argv) != 2:
        print("Usage: ./<script_name> <full/path/to/csvfile.csv>")
        return
    path_to_file = sys.argv[1]
    csv_data = CsvOperations().read_csv_data(path_to_file=path_to_file)
    if not csv_data:
        return
    store_csv_data(csv_data)

    # Todo: Inform which line was not inserted due to validation failure.
    print("Successfully stored {} records!".format(len(csv_data)))


if __name__ == '__main__':
    main()
