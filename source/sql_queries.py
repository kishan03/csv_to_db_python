from common_constants import CSV_TABLE_NAME

# SQL statements for csv_file_data table
SQLITE_CREATE_TABLE_SQL = """CREATE TABLE IF NOT EXISTS {table_name} (
                                        id integer PRIMARY KEY AUTOINCREMENT,
                                        country text NOT NULL,
                                        age int NOT NULL,
                                        salary int NOT NULL,
                                        purchased bool NOT NULL
                                    );""".format(table_name=CSV_TABLE_NAME)

INSERT_INTO_SQL = "INSERT INTO {} {} VALUES (?, ?, ?, ?)".format(
    CSV_TABLE_NAME,
    ('country', 'age', 'salary', 'purchased'))
