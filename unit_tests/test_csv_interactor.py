import os
import unittest

from source.csv_interactor import CsvOperations


class TestCSVOperations(unittest.TestCase):
    def setUp(self):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.csv_operations = CsvOperations()

    def test_read_csv_data_empty_file(self):
        empty_file = self.dir_path + "/dummy_data/empty.csv"

        assert self.csv_operations.read_csv_data(path_to_file=empty_file) is None

    def test_read_csv_data_nonexisting_file(self):
        nonexisting_file = self.dir_path + "/dummy_data/nonexisting.csv"

        assert self.csv_operations.read_csv_data(path_to_file=nonexisting_file) is None

    def test_read_csv_data_good_file(self):
        good_data = self.dir_path + "/dummy_data/good_data.csv"
        assert len(self.csv_operations.read_csv_data(path_to_file=good_data)) == 3

    def test_is_valid_row_bad_inputs(self):
        assert self.csv_operations.is_valid() is False
        assert self.csv_operations.is_valid(row=[]) is False
        assert self.csv_operations.is_valid(row="Hello") is False
        assert self.csv_operations.is_valid(row={"hi": "noddus!"}) is False
        assert self.csv_operations.is_valid(row=1) is False

        assert self.csv_operations.is_valid(row=[1, 2, 3]) is False
        assert self.csv_operations.is_valid(row=[1, '2', 3]) is False
        assert self.csv_operations.is_valid(row=['1', '2', '3', 4]) is False

    def test_is_valid_row_good_inputs(self):
        assert self.csv_operations.is_valid(row=['1', '2', '3', '4']) is True  # Todo: Improve validation
        assert self.csv_operations.is_valid(row=['France', '2', '3', 'True']) is True
